import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SigninComponent } from './components/signin/signin.component';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { InfluenceurComponentComponent } from './components/influenceur-component/influenceur-component.component';
import { TendanceComponentComponent } from './components/tendance-component/tendance-component.component';
import { NotificationComponentComponent } from './components/notification-component/notification-component.component';
import { SearchBarComponentComponent } from './components/search-bar-component/search-bar-component.component';
import { TextAreaComponentComponent } from './components/text-area-component/text-area-component.component';
import { PublicationComponent } from './components/publication/publication.component';
import { HomeComponent } from './components/home/home.component';
import { MyAccountComponent } from './components/my-account/my-account.component';
import { FriendsComponent } from './components/friends/friends.component';
import { MessageComponent } from './components/message/message.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SigninComponent,
    NavbarComponent,
    InfluenceurComponentComponent,
    TendanceComponentComponent,
    NotificationComponentComponent,
    SearchBarComponentComponent,
    TextAreaComponentComponent,
    PublicationComponent,
    HomeComponent,
    MyAccountComponent,
    FriendsComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,   
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
