import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  deconnexion(){
    this.router.navigate(['']);
  }

  flux(){
    this.router.navigate(['home']);
  }

  monCompte(){
    this.router.navigate(['Account']);
  }

  ami(){
    this.router.navigate(['friends']);
  }
}
