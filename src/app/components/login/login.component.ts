import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    email: ['',Validators.required],
    password: ['',Validators.required]
  });

  affichageAlert = false;

  constructor(private router : Router,
              private fb: FormBuilder) {}

  ngOnInit() {}

  connexion() {
    if(this.loginForm.value.email == 'root@root.fr' && this.loginForm.value.password == "root"){
      this.router.navigate(['/home']);
    } else {
      this.affichageAlert = true;
    }
  }

  signin(){
    this.router.navigate(['signin']);
  }
}
