import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluenceurComponentComponent } from './influenceur-component.component';

describe('InfluenceurComponentComponent', () => {
  let component: InfluenceurComponentComponent;
  let fixture: ComponentFixture<InfluenceurComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluenceurComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluenceurComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
