import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TendanceComponentComponent } from './tendance-component.component';

describe('TendanceComponentComponent', () => {
  let component: TendanceComponentComponent;
  let fixture: ComponentFixture<TendanceComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TendanceComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TendanceComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
